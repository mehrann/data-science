import matplotlib.pyplot as plt
import numpy as np
import csv
#import graphviz
import dataManip

#from graphviz import Digraph

class FP_Tree(): 
	class Node():
		# on utilise __slots__, car l'accès aux attributs sera plus rapide et prendra moins de mémoire
		__slots__ = 'children', 'parent', 'element', 'count', 'pointer'
		def __init__(self, element, count=1):  
			self.parent = None
			self.children = []
			self.element = element
			self.count = count
			self.pointer = None
	
	def __init__(self):
		# on initialise la racine et place la tête de lecture à la racine 
		self._root = self.Node(None)
		self._head = self._root
		self._nb_node = 1
		
		# départ et fin des listes chainées
		self._end_list = {}
		self._start_list = {}
		
		# utiles pour retrouver les patrons
		self.itemsets = []
		
	def __len__(self):
		return self._nb_node
	
	def insert(self, table, count=1):
		"""Insert persons in the tree

		Keyword arguments:
		table -- a list of persons
		"""
		
		self._head = self._root
		
		for person in table:
			new_node = True
			for child in self._head.children:
				# si le produit est déjà dans l'arbre:
				if person == child.element:
					new_node = False
					self._head = child
					# on augmente le compte de ce produit
					self._head.count += count
			# si le noeud n'est pas présent, on l'ajoute
			if new_node:
				self._nb_node += 1
				node = self.Node(person, count)
				node.parent = self._head
				self._head.children.append(node)
				self._head = node
				# on ajoute un pointeur vers ce nouvel élément
				# s'il est déjà présent dans l'arbre
				if person in self._end_list:
					self._end_list[person].pointer = self._head
				else:
					self._start_list[person] = self._head
				self._end_list[person] = self._head
				
	def remove(self, node):
		"""Remove a node in the tree making sure to keep the links
		   relevant (parent-child and pointer)

		Keyword arguments:
		node -- a reference to the node that will be removed
		"""
			
		# on met à jour tout d'abord les liens parent-enfant
		# si le noeud n'a pas d'enfant:
		if not node.children:
			node.parent.children.remove(node)            
		else:
			node.parent.children.remove(node)
			node.parent.children.extend(node.children)
			
		# on met à jour les pointeurs
		cur_node = self._start_list[node.element]
		# si le noeud cherché est le premier de la liste
		if cur_node is node:
			if node.pointer is None:
				del self._start_list[node.element]
			else:
				self._start_list[node.element] = node.pointer
		else:
			while cur_node.pointer is not node:
				cur_node = cur_node.pointer              
			cur_node.pointer = node.pointer       
	
	def _tree_traversal(self, node):
		for n in node.children:
			self.dot.node(str(hash(n)), str(n.element) + ":" + str(n.count))
			self.dot.edge(str(hash(node)), str(hash(n)))
		
		for child in node.children:
			self._tree_traversal(child)
			
	def _pointer_traversal(self):
		for person, _ in self._start_list.items():
			node = self._start_list[person]
			
			while node.pointer != None:
				self.dot.edge(str(hash(node)), str(hash(node.pointer)), style="dashed", constraint='false')
				node = node.pointer
	
	def show(self, with_pointer=False):
		self.counter = 0
		self.dot = Digraph()
		self.dot.attr(rankdir='TB', size='8,12')
		self.dot.node(str(hash(self._root)), 'ROOT')
		self._tree_traversal(self._root)
		if with_pointer:
			self._pointer_traversal()
		return self.dot
	  
	def get_count(self, person):
		"""Count the frequency of a person by following the linked list

		Keyword arguments:
		person -- the number of the person to count
		"""
		node = self._start_list[person]
		count = node.count
		while node.pointer != None:
			node = node.pointer
			count += node.count
		return count
	
	def get_parents(self, node):
		"""Return the list of parents of a given node

		Keyword arguments:
		node -- a Node object
		"""
		branch = []
		while node.parent.element is not None:
			branch.append(node.parent.element)
			node = node.parent
		branch.sort()
		return branch
	
	def get_conditional_tree(self, person, s):
		"""Return the conditional tree of a person. To do so, it removes
		   the given person, update the counts and remove not common nodes

		Keyword arguments:
		person -- the number of a person
		s -- an integer, the threshold to determine if a person is common
		"""
			
		# premier noeud
		tree = FP_Tree()
		node = self._start_list[person]
		
		branch = self.get_parents(node)
		tree.insert(branch, node.count)
		
		# tous les autres noeuds de la liste chainées:
		while node.pointer != None:
			node = node.pointer
			branch = self.get_parents(node)
			tree.insert(branch, node.count)
			
		# enlève les noeuds ayant un compte trop bas
		"""
		for person, _ in tree._start_list.items():
			if tree.get_count(person) <= s:
				node = tree._start_list[person]
				
				while node.pointer is not None:
					tmp_node = node.pointer
					tree.remove(node)
					node = tmp_node
				tree.remove(node)
		"""
				
		return tree
	
	def extract(self, prefix, tree, s=2):
		"""Find all the itemsets given a threshold s

		Keyword arguments:
		prefix -- list of persons in the present itemset
		tree -- an FP_Tree object. This is usually the conditional tree
		s -- an integer, the threshold to determine if a person is common
		"""
		for person, _ in tree._start_list.items():
			if tree.get_count(person) > s:
				self.itemsets.append(prefix + [person])
				new_tree = tree.get_conditional_tree(person, s)
				self.extract(prefix + [person], new_tree)
				
	
def lookForPatrons(csv_file,size):
	freq = {}
	history = []
	resencement = {}
	idx = 0

	with open(csv_file) as csv_file:
		reader = csv.reader(csv_file, delimiter=',')
		for i, row in enumerate(reader):            
			history.append(row[:-1])
			
			for person in row[:-1]:
				if person not in resencement:
					resencement[person] = idx
					freq[idx] = 1
					idx += 1
				else:
					freq[resencement[person]] += 1
		
	s = 100
	frequent_person = [k for k, v in freq.items() if v > s]

	def test(subset=10):
		fp_tree = FP_Tree()

		for table in history[:subset]:
			table.sort()
			#on insère dans le trie un table
			fp_tree.insert(table)
		return fp_tree

	tree = test(size)
	#tree = test(200)

	tree.extract([], tree)
	return tree.itemsets
	#print(tree.itemsets)
	
	
fp_tree = lookForPatrons('adultSansCapitalMoins50k.csv', None)
print(len(fp_tree))
#freq = {}
#history = []
#resencement = {}
#idx = 0
#
#with open('adultSansCapitalMoins50k.csv') as csv_file:
#	reader = csv.reader(csv_file, delimiter=',')
#	for i, row in enumerate(reader):            
#		history.append(row[:-1])
#		
#		for person in row[:-1]:
#			if person not in resencement:
#				resencement[person] = idx
#				freq[idx] = 1
#				idx += 1
#			else:
#				freq[resencement[person]] += 1
#	
#s = 100
#frequent_person = [k for k, v in freq.items() if v > s]
#
#def test(subset=10):
#	fp_tree = FP_Tree()
#
#	for table in history[:subset]:
#		table.sort()
#		#on insère dans le trie un table
#		fp_tree.insert(table)
#	return fp_tree
#
#tree = test(200)
#
#tree.extract([], tree)
#print(tree.itemsets)

#csv_file = np.loadtxt('adult.csv')			
#reader = csv.reader(csv_file, delimiter=' ')
#for i, row in enumerate(reader):            
#	history.append(row[:-1])	
#	for person in row[:-1]:
#		if person not in resencement:
#			resencement[person] = idx
#			freq[idx] = 1
#			idx += 1
#		else:
#			freq[resencement[person]] += 1
		
#dot = Digraph(comment='The Round Table')
#dot.node('A', 'King Arthur')
#dot.node('B', 'Sir Bedevere the Wise')
#dot.node('L', 'Sir Lancelot the Brave')
#
#dot.edges(['AB', 'AL'])
#dot.edge('B', 'L', constraint='false')
#graphviz.Source(dot)
#dot.render('round-table.gv', view=True) 

#tree = test()
#
## on affiche le trie résultant
#dot = tree.show()
#graphviz.Source(dot)
#dot.render('table.gv', view=True)