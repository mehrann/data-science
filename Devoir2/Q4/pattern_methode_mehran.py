import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from apyori import apriori
from mlxtend.preprocessing import TransactionEncoder
from mlxtend.frequent_patterns import fpgrowth
import pyfpgrowth
import dataManipTest

#result = [[1,2,5],[1,2,5],[1,4,6]]
result = pd.read_csv("adult.csv")
#isdigit = False

dataset = []
#for tirage in result:
#	dataset.append([str(nombre) for nombre in tirage])

#if isdigit:
#	dataset = []
for tirage in result:
	if not isinstance(tirage, str):
		dataset.append([str(nombre) for nombre in tirage])
	else: 
		dataset.append(tirage)
#else:
#	dataset = result.copy()

te = TransactionEncoder()
te_ary = te.fit(dataset).transform(dataset)
df = pd.DataFrame(te_ary, columns=te.columns_)

arbre = fpgrowth(df, min_support=0.1, use_colnames=True)
arbre["length"]= arbre["itemsets"].str.len() 


#Technique 2
patterns = pyfpgrowth.find_frequent_patterns(dataset, 2)
rules = pyfpgrowth.generate_association_rules(patterns,0.8)

#Pattern
ttmp = pd.DataFrame(patterns.items(), columns=["data", "freq"])
ttmp["length"]= ttmp["data"].str.len()
#Rule
ruleTtmp = pd.DataFrame(rules.items(), columns=["from", "to"])
ruleTtmp["freq"]= ruleTtmp["to"].str[1]
ruleTtmp["dimFrom"] = ruleTtmp["from"].str.len()
ruleTtmp["dimTo"] = ruleTtmp["to"].str.len()

print("test")
#print(result.reset_index(drop=True).Low)

print("DataFrame")
print(result)

print("Arbre-FP")
print(arbre[arbre["length"]>=2])

print(ttmp.head())
print(ttmp)
print(ruleTtmp.head())