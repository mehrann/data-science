import matplotlib.pyplot as plt
import numpy as np
import csv
import graphviz
from graphviz import Digraph

class FP_Tree(): 
	class Node():
		# on utilise __slots__, car l'accès aux attributs sera plus rapide et prendra moins de mémoire
		__slots__ = 'children', 'parent', 'element', 'count', 'pointer'
		def __init__(self, element, count=1):  
			self.parent = None
			self.children = []
			self.element = element
			self.count = count
			self.pointer = None
	
	def __init__(self):
		# on initialise la racine et place la tête de lecture à la racine 
		self._root = self.Node(None)
		self._head = self._root
		self._nb_node = 1
		
		# départ et fin des listes chainées
		self._end_list = {}
		self._start_list = {}
		
		# utiles pour retrouver les patrons
		self.itemsets = []
		
	def __len__(self):
		return self._nb_node
	
	def insert(self, basket, count=1):
		"""Insert products in the tree

		Keyword arguments:
		basket -- a list of products
		"""
		
		self._head = self._root
		
		for product in basket:
			new_node = True
			for child in self._head.children:
				# si le produit est déjà dans l'arbre:
				if product == child.element:
					new_node = False
					self._head = child
					# on augmente le compte de ce produit
					self._head.count += count
			# si le noeud n'est pas présent, on l'ajoute
			if new_node:
				self._nb_node += 1
				node = self.Node(product, count)
				node.parent = self._head
				self._head.children.append(node)
				self._head = node
				# on ajoute un pointeur vers ce nouvel élément
				# s'il est déjà présent dans l'arbre
				if product in self._end_list:
					self._end_list[product].pointer = self._head
				else:
					self._start_list[product] = self._head
				self._end_list[product] = self._head
				
	def remove(self, node):
		"""Remove a node in the tree making sure to keep the links
		   relevant (parent-child and pointer)

		Keyword arguments:
		node -- a reference to the node that will be removed
		"""
			
		# on met à jour tout d'abord les liens parent-enfant
		# si le noeud n'a pas d'enfant:
		if not node.children:
			node.parent.children.remove(node)            
		else:
			node.parent.children.remove(node)
			node.parent.children.extend(node.children)
			
		# on met à jour les pointeurs
		cur_node = self._start_list[node.element]
		# si le noeud cherché est le premier de la liste
		if cur_node is node:
			if node.pointer is None:
				del self._start_list[node.element]
			else:
				self._start_list[node.element] = node.pointer
		else:
			while cur_node.pointer is not node:
				cur_node = cur_node.pointer              
			cur_node.pointer = node.pointer       
	
	def _tree_traversal(self, node):
		for n in node.children:
			self.dot.node(str(hash(n)), str(n.element) + ":" + str(n.count))
			self.dot.edge(str(hash(node)), str(hash(n)))
		
		for child in node.children:
			self._tree_traversal(child)
			
	def _pointer_traversal(self):
		for product, _ in self._start_list.items():
			node = self._start_list[product]
			
			while node.pointer != None:
				self.dot.edge(str(hash(node)), str(hash(node.pointer)), style="dashed", constraint='false')
				node = node.pointer
	
	def show(self, with_pointer=False):
		self.counter = 0
		self.dot = Digraph()
		self.dot.attr(rankdir='TB', size='8,12')
		self.dot.node(str(hash(self._root)), 'ROOT')
		self._tree_traversal(self._root)
		if with_pointer:
			self._pointer_traversal()
		return self.dot
	  
	def get_count(self, product):
		"""Count the frequency of a product by following the linked list

		Keyword arguments:
		product -- the number of the product to count
		"""
		node = self._start_list[product]
		count = node.count
		while node.pointer != None:
			node = node.pointer
			count += node.count
		return count
	
	def get_parents(self, node):
		"""Return the list of parents of a given node

		Keyword arguments:
		node -- a Node object
		"""
		branch = []
		while node.parent.element is not None:
			branch.append(node.parent.element)
			node = node.parent
		branch.sort()
		return branch
	
	def get_conditional_tree(self, product, s):
		"""Return the conditional tree of a product. To do so, it removes
		   the given product, update the counts and remove not common nodes

		Keyword arguments:
		product -- the number of a product
		s -- an integer, the threshold to determine if a product is common
		"""
			
		# premier noeud
		tree = FP_Tree()
		node = self._start_list[product]
		
		branch = self.get_parents(node)
		tree.insert(branch, node.count)
		
		# tous les autres noeuds de la liste chainées:
		while node.pointer != None:
			node = node.pointer
			branch = self.get_parents(node)
			tree.insert(branch, node.count)
			
		# enlève les noeuds ayant un compte trop bas
		"""
		for product, _ in tree._start_list.items():
			if tree.get_count(product) <= s:
				node = tree._start_list[product]
				
				while node.pointer is not None:
					tmp_node = node.pointer
					tree.remove(node)
					node = tmp_node
				tree.remove(node)
		"""
				
		return tree
	
	def extract(self, prefix, tree, s=2):
		"""Find all the itemsets given a threshold s

		Keyword arguments:
		prefix -- list of products in the present itemset
		tree -- an FP_Tree object. This is usually the conditional tree
		s -- an integer, the threshold to determine if a product is common
		"""
		for product, _ in tree._start_list.items():
			if tree.get_count(product) > s:
				self.itemsets.append(prefix + [product])
				new_tree = tree.get_conditional_tree(product, s)
				self.extract(prefix + [product], new_tree)
				
	def test(subset=10):
		fp_tree = FP_Tree()

		for basket in history[:subset]:
			# on convertit les noms de produit en entier
			basket = [market[product] for product in basket if market[product] in frequent_product]
			# on trie les produits
			basket.sort()
			#on insère dans le trie un basket
			fp_tree.insert(basket)
		return fp_tree
		

freq = {}
history = []
market = {}
idx = 0

with open('commande.txt') as csv_file:
	reader = csv.reader(csv_file, delimiter=' ')
	for i, row in enumerate(reader):            
		history.append(row[:-1])
		
		for product in row[:-1]:
			if product not in market:
				market[product] = idx
				freq[idx] = 1
				idx += 1
			else:
				freq[market[product]] += 1
				
s = 100
frequent_product = [k for k, v in freq.items() if v > s]

#dot = Digraph(comment='The Round Table')
#dot.node('A', 'King Arthur')
#dot.node('B', 'Sir Bedevere the Wise')
#dot.node('L', 'Sir Lancelot the Brave')
#
#dot.edges(['AB', 'AL'])
#dot.edge('B', 'L', constraint='false')
#graphviz.Source(dot)
#dot.render('round-table.gv', view=True) 

def test(subset=10):
	fp_tree = FP_Tree()

	for basket in history[:subset]:
		# on convertit les noms de produit en entier
		basket = [market[product] for product in basket if market[product] in frequent_product]
		# on trie les produits
		basket.sort()
		#on insère dans le trie un basket
		fp_tree.insert(basket)
	return fp_tree

#tree = test()
#
## on affiche le trie résultant
#dot = tree.show()
#graphviz.Source(dot)
#dot.render('basket.gv', view=True)

tree = test(10)

tree.extract([], tree)
print(tree.itemsets)