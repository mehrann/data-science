import pandas as pd
import numpy as np

df = pd.read_csv("adult.csv")

df_modified = df.drop(columns=['capital-loss', 'capital-gain','fnlwgt'])
dfLessCapitalLoss = df_modified[df_modified['occupation'] != '?'] #Cleanup des valeurs inconnues
print(df.head())
print(dfLessCapitalLoss)


#Saving data frames
dfLessCapitalLoss.to_csv('adultSansCapitalMoins50k.csv')

