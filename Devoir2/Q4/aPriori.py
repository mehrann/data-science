import pandas as pd
import time
import numpy as np
from itertools import combinations
import random

df = pd.read_csv("adult.csv")

'''
	fnlwgt serait quelques chose de similaire a un Identifiant personnel, on estime que l'on en a pas besoin dans nos donnees. 
	De meme qie la colonne 'education-num' puisque nous pouvons deja connaitre le niveua de scolarite de l'individu puisque celui-ci est explicite
'''
df.drop(['fnlwgt', 'educational-num'], axis=1, inplace=True)
df.head()

'''
	Le jeu de donnees possede des valeurs manquantes qui sont representees par des '?'. 
	On decide (arbitrairement) de les retirer dans presque toutes les colonnes sauf celles du pays d'origine pour ne pas 'polluer' nos donnees
'''
df = df.replace(' ?', np.NaN)
df.dropna(axis = 0, inplace = True)
df.reset_index(drop= True, inplace= True)
df.head(50)


'''
	Pour pouvoir appliquer l'algorithme A-Priori sur notre jeu de donnees, nous devont transmorfer certaines 
	donnees en donnees categorielles, pour pouvoir generer des regles d'association. 
	On convertit donc des variables continues et non explicites en variables discretes. 
	Notons que nous perdons bien evidemment en precision
'''

df['age'] = pd.cut(df['age'], bins = [0, 25, 50, 100],
						 labels = ['Jeune-Adulte', 'Adulte', 'Aine'],
						 right = True, include_lowest = True)

df['hours-per-week'] = pd.cut(df['hours-per-week'], [0, 30, 45, 100],
										labels = ['Temps_partiel', 'Temps_plein', 'Temps_supplementaire'],
										right = True, include_lowest = True)

df['capital-gain'] = pd.cut(df['capital-gain'], [0, 1, 50000, 100000],
									 labels = ['No_capital_Gain', 'Medium_capital_Gain', 'High_capital_Gain'],
									 right = True, include_lowest = True)

df['capital-loss'] = pd.cut(df['capital-loss'], [0, 1, 50000, 100000],
									 labels = ['No_capital_Loss', 'Medium_capital_loss', 'High_capital_Loss'],
									 right = True, include_lowest = True)
df.head()

'''
	Enfin nous allons diviser et etudier le comprotement de A-priori sur divers jeux de donnees car,
	comme nous l'avons vu plus haut et a la demonstration 8, le jeu de donnees est severement debalance.
	La population Nord-Americaine y est sur-representee, ainsi que la population Caucasienne masculine ayant
	un salaire <= 50K. Nous allons donc faires quelques divisions des ensembles de donnees pour observer 
	les differents patrons qui seront generes.
'''

# DataFrame avec les donnees des individus gagnant plus de >50k
dfOver50K = df[df['income'] == ">50K"]


#Population hors USA
dfLessUSA = df[df['native-country'] != "United-States"]


#Population hors USA gagnant plus de >50k
dfLessUSAOver50K = dfLessUSA[dfLessUSA['income'] == ">50K"]


#Population hors USA gagnant moins de <=50k
dfLessUSALess50K = dfLessUSA[dfLessUSA['income'] == "<=50K"]


#Population hors USA Feminine
dfLessUSAFemale = dfLessUSA[dfLessUSA['gender'] != "Male"]


#Population hors USA Feminine gagant un salaire de plus de >50k
dfLessUSAFemaleLess50K = dfLessUSAFemale[dfLessUSAFemale['income'] == "<=50K"]


#Population hors USA Feminine gagant un salaire de plus de >50k
dfLessUSAFemaleOver50K = dfLessUSAFemale[dfLessUSAFemale['income'] == ">50K"]


#Population non caucasienne 
dfLessCaucasian = df[df['race'] != "White"]


#Population non caucasienne <=50K
dfLessCaucasianLess50K = dfLessCaucasian[dfLessCaucasian['income'] == "<=50K"]


#Population non caucasienne >50K
dfLessCaucasianOver50K = dfLessCaucasian[dfLessCaucasian['income'] == ">50K"]


#Population hors USA non caucasienne <=50K
temp = dfLessUSA[dfLessUSA['income'] == "<=50K"]
dfLessCaucasianLessUSALess50K = temp[temp['race'] != "White"]


#Population hors USA non caucasienne >50K
temp = dfLessUSA[dfLessUSA['income'] == ">50K"]
dfLessCaucasianLessUSAOver50K = temp[temp['race'] != "White"]

dfOver50K.drop(['income'], axis=1, inplace=True)
dfLessUSA.drop(['income'], axis=1, inplace=True)
dfLessUSAOver50K.drop(['income'], axis=1, inplace=True)
dfLessUSALess50K.drop(['income'], axis=1, inplace=True)
dfLessUSAFemale.drop(['income'], axis=1, inplace=True)
dfLessUSAFemaleLess50K.drop(['income'], axis=1, inplace=True)
dfLessUSAFemaleOver50K.drop(['income'], axis=1, inplace=True)
dfLessCaucasian.drop(['income'], axis=1, inplace=True)
dfLessCaucasianLess50K.drop(['income'], axis=1, inplace=True)
dfLessCaucasianOver50K.drop(['income'], axis=1, inplace=True)
dfLessCaucasianLessUSALess50K.drop(['income'], axis=1, inplace=True)
dfLessCaucasianLessUSAOver50K.drop(['income'], axis=1, inplace=True)
print(dfLessUSA.head())

def get_candidates(dataFrame):

	temp_candidates = {}

	for index, row in dataFrame.iterrows():
		# Convertit chaque ligne du jeu de donnees en une transaction
		transaction = list(set(row))
		'''
			Chaque element de la transaction est un item. Le mombre de chaque item est contenu dans
			un dictionnaire (key,value)
		'''
		for item in transaction:
			if (item in temp_candidates.keys()):
				temp_candidates[item] += 1
			else:
				temp_candidates[item] = 1
	return temp_candidates
	
def generateLevelOneSubstets(d, c, minSupport):

	for key,value in c.items():
		if (value >= minSupport):
			d[key] = value
	return d
	
def join(frequentItemsets, k):
	cand_sets = []
	total_items = list(frequentItemsets.keys())
	singleUniqueElements = set()
	if (k>2):
		for item in set(total_items):
			singleUniqueElements = singleUniqueElements.union(item)
	else:
		singleUniqueElements = set(total_items)

	cand_sets = list(combinations(singleUniqueElements, k))

	return cand_sets


#On efface les sous-ensemble peu frequents
def has_infrequent_subset(item, prevFreqSet, k):
	
	candSubsets = set(combinations(item,(k-1)))
	
	if (not(candSubsets.issubset(prevFreqSet))):
		return True

	return False

#Compte le support de chaque transaction candidate
def getSupportCount(candidateItemsets,dataFrame):
	
	candidateSets = {key:0 for key in candidateItemsets}
	
	for key,value in candidateSets.items():
		for index,row in dataFrame.iterrows():
			if(set(key).issubset(set(row))):
				candidateSets[key] += 1
				
	return candidateSets

def afficherCandidats(k, candidateItemsets, previousFrequentItemsets, minSupport, dataFrame):
	
	candidates ={}
	prevFreqSet = set(previousFrequentItemsets)

	for item in candidateItemsets:
		if(has_infrequent_subset(item, prevFreqSet, k)):
			candidateItemsets.remove(item)

	candidates = getSupportCount(candidateItemsets,dataFrame)
	print("Ensembles candidats:\n", candidates)
	
	return candidates

#Genere les sous-ensemble de niveau-K
def generateLevelKSubstets(candidateItemsets, minSupport):
	
	frequentItemsets = {}
	
	for item,count in candidateItemsets.items():
		if (count >= minSupport):
			frequentItemsets[item] = count

	return frequentItemsets

def aPriori(dataFrame, minSupportPercentage):

	minSupport = round((minSupportPercentage / 100.0) * len(dataFrame.index))
	print ('Minimum Support Count = ', minSupport)
	
	# Generating the 1st set of candidates C1
	candidateItemsets = get_candidates(dataFrame)
	
	# Generating 1st set of frequent itemsets L1
	frequentItemsets = {}
	frequentItemsets = generateLevelOneSubstets(frequentItemsets, candidateItemsets, minSupport)

	#totalFrequentItemsets = len(frequentItemsets)
	iteration = 1
	
	finalFrequentItemsets = {}
	finalFrequentItemsets.update(frequentItemsets)

	print('Ensemble de caracteristiques frequentes N-1 : \n')
	for key,value in frequentItemsets.items():
		print(key, ' : ', value)
	
	while True:
		iteration += 1
		candidateItemsets = join(frequentItemsets, iteration)
		candidateItemsets = afficherCandidats(iteration, candidateItemsets,
										  frequentItemsets, minSupport, dataFrame)
		frequentItemsets = generateLevelKSubstets(candidateItemsets, minSupport)
		finalFrequentItemsets.update(frequentItemsets)
		
		if (bool(frequentItemsets)):
			print('Ensemble de caracteristiques frequentes N-',iteration, 'iterations: \n')
			for key,value in frequentItemsets.items():
				print(key, ' : ', value, '\n')
		if len(frequentItemsets) == 0:
			print('All frequent patterns: \n')
			for key,value in finalFrequentItemsets.items():
				print(key, ' : ', value)
			break
	return
	
#Algorithme A-priori donnant un meilleur resultat en definissant un seuil de support minimal
# Et en utilisant un facteur aleatoire pour melanger la selection des donnees
def sampled_apriori(df, minSup, samplingFactor):
	
	#Perform a random sample of the dataframe
	df = df.sample(frac = samplingFactor).reset_index(drop = True)
	
	# We lower the minimum support in hope of capturing all global frequent patterns
	minSup = minSup*0.9
	
	startTime = time.time()
	aPriori(df, minSup)
	totalRunTime = time.time() - startTime
	print ('\n\n Total Execution Time: ', totalRunTime)

def benchmark_data(df,minSupport, samplingFactor):
	startTime = time.time()
	aPriori(df,60)
	totalRunTime = time.time() - startTime
	print ('\n Total Execution Time: ', totalRunTime)
	sampled_apriori(df,minSupport,samplingFactor)

#benchmark_data(df,70,0.5)
#benchmark_data(dfOver50K,70,0.5)
#benchmark_data(dfLessUSA,70,0.5)
#benchmark_data(dfLessUSALess50K,70,0.5)
#benchmark_data(dfLessUSAOver50K,70,0.5)
#benchmark_data(dfLessUSAFemale,70,0.5)
#benchmark_data(dfLessUSAFemaleLess50K,70,0.5)
#benchmark_data(dfLessUSAFemaleOver50K,70,0.5)
#benchmark_data(dfLessCaucasian,70,0.5)
#benchmark_data(dfLessCaucasianLess50K,70,0.5)
#benchmark_data(dfLessCaucasianOver50K,70,0.5)
#benchmark_data(dfLessCaucasianLessUSALess50K,70,0.5)
#benchmark_data(dfLessCaucasianLessUSAOver50K,70,0.5)
#benchmark_data(df,70,0.5)
#benchmark_data(df,70,0.5)


