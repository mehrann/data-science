import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
df = pd.read_csv("adult.csv")
df_modified = df


"""
	Cleanup de toutes les valeurs manquantes, on ne retrouve plus '?' dans le dataFrame
"""
df_modified = df_modified[df_modified['age'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['workclass'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['fnlwgt'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['education'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['educational-num'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['marital-status'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['occupation'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['relationship'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['occupation'] != '?'] #Cleanup des valeurs inconnues
df_modified = df_modified[df_modified['native-country'] != '?'] #Cleanup des valeurs inconnues

#Table ne contenant plus que les elements non-fonciers ainsi que la moyenne de la population <=50k
dfLessCapitalLoss = df_modified.drop(columns=['capital-loss', 'capital-gain','fnlwgt'])
dfLessCapitalLoss.to_csv('adultSansCapital.csv')

dfLessCapitalLossLess50 = dfLessCapitalLoss[dfLessCapitalLoss['income'] == "<=50K"]
dfLessCapitalLossLess50.to_csv('adultSansCapitalMoins50k.csv')

#table ne contenant plus que les elements non-fonciers ainsi que la moyenne de la population >50k
dfLessCapitalLossOver50 = dfLessCapitalLoss[dfLessCapitalLoss['income'] == ">50K"]
dfLessCapitalLossOver50.to_csv('adultSansCapitalPLus50k.csv')

#Table ne contenant plus que les donnees non ordinales <=50k
dfSimpleLifeDataLess50 = dfLessCapitalLossLess50



#Table ne contenant plus que les donnees non ordinales >50k
dfSimpleLifeDataOver50 = dfLessCapitalLossOver50

# Table ne contenant plus que 
#print(df.head())
#print(df_modified)
#print(dfLessCapitalLossLess50)
#print(dfLessCapitalLossOver50)